import math
class Model:
    def __init__(self):
        self.array = [[" "," "," "],[" "," "," "],[" "," "," "]]
        self.num = 0
    def check_player(self):
        if (self.num%2==0):
            return "X"
        else:
            return "O"
    def check_error(self,x,y): #error if array out of bound AND if array is used
        if (x < 3 and y < 3 and x >= 0 and y >= 0):
            if(self.array[y][x]==" "):
                return True
            else:
                print("------------try again------------")
                print()
                return False
        else:
            print("------------try again-------------")
            print()
            return False
    def get_turn(self):
        return self.num
    def set_turn(self,num):
        self.num=num
    def set_array(self,x,y,player):
        self.array[y][x] = player
    def check_winner(self):
        k = 0
        array = self.array
        while k<3:
            if (array[k][0] == array[k][1] == array[k][2] == self.check_player()):
                print (self.check_player()," WIN")
                self.num = 10 #num=10 to break loop
            if (array[0][k] == array[1][k] == array[2][k] == self.check_player()):
                print (self.check_player()," WIN")
                self.num = 10
            k+=1
        if(array[0][0] == array[1][1] == array[2][2] == self.check_player()):
            print (self.check_player()," WIN")
            self.num = 10
        if(array[0][2] == array[1][1] == array[2][0] == self.check_player()):
            print (self.check_player()," WIN")
            self.num = 10

class Controller: #for input row and column
    def __init__ (self,m,v):
        self.m = m
        self.v = v
    def select_mode(self):
        print("Select mode to play game")
        mode = int(input("1 = mode row and column , 2 = mode position:"))
        if(mode == 1):
            c = Controller1(self.m,self.v)
            c.start()
        elif(mode == 2):
            c = Controller2(self.m,self.v) 
            c.start()
        else:
            self.select_mode()
    def check_restart(self):
        print("Do you want to restart game")
        start = int(input("Yes = 1 , No = 0:"))
        if(start == 1):
            self.m.set_turn(0)
            i = 0
            j = 0
            while i < 3:
                j = 0
                while j < 3:
                    self.m.set_array(j,i," ")
                    j += 1
                i += 1
            self.select_mode()
        elif(start != 0):
            self.check_restart()
class Controller1(Controller):
    def __init__(self,m,v):
        Controller.__init__(self, m, v)
    def start(self):
        turn=self.m.get_turn()
        while (turn<9):
            try:
                print(">> ",self.m.check_player()," Turn") #Show player turn
                x = int(input("input ROW (0-2):"))
                y = int(input("input COLUMN (0-2):"))
                self.add_item(x,y) #only update num if it can add item
                self.v.displayTable()
            except ValueError:
                print()
                print ("----------------not a number---------------")
                print()
            turn=self.m.get_turn()
        if(turn==9):
            print("Draw")
        self.check_restart()
    def add_item(self,x,y):
        if(self.m.check_error(x,y)==True): 
            self.m.set_array(x,y,self.m.check_player())
            self.m.check_winner()
            self.m.set_turn(self.m.get_turn()+1)
class Controller2(Controller): #for input 1-9
    def __init__(self,m,v):
        Controller.__init__(self, m, v)
    def add_item(self, position): 
        y=math.ceil(position/3)-1
        x=position-1-(3*y)
        if(self.m.check_error(x,y)==True): 
            self.m.set_array(x,y,self.m.check_player())
            self.m.check_winner()
            self.m.set_turn(self.m.get_turn()+1)
    def start(self):
        turn=self.m.get_turn()
        while (turn<9):
            try:
                print(">> ",self.m.check_player()," Turn") #Show player turn
                position = int(input("Please enter position :"))
                self.add_item(position) #only update num if it can add item
                self.v.displayTable()
            except ValueError:
                print()
                print ("-------------not a number--------------")
                print()
            turn=self.m.get_turn()
        if(turn==9):
            print("Draw")
        self.check_restart()
        
class Viewer:
    def __init__(self,model):
        self.m = model
    def displayTable(self):
        i = 0
        j = 0
        while i < 3:
            j = 0
            while j < 3:
                print (self.m.array[i][j],end=' ')
                j += 1
            print()
            i += 1

def setup():
    m = Model()
    v = Viewer(m)
    c = Controller(m,v)   
    c.select_mode()

setup()